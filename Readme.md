# Arquitectura

Este proyecto se encuentra estructurado bajo el patron MVVM, se escogio esta arquitectura ya que el trabajo queda dividido de una forma mas fluida, sencilla y entendible, ademas permite manejar de una mejor manera los cambios de la interfaz, tambien se hizo uso de patrones de diseño como lo fue el patron singleton y el patron DAO.

Para el manejo de la infomación y su almacenamiento se utilizo ROOM esto porque es mas sencillo de usar y tambien porque permite hacer las consultas de una forma simple con sqlite.

Tambien se hizo uso de observables para el manejo de las respuestas dadas por las consultas y para el llamado a la API se uso la libreria retrofit2.

Para usar y probar el proyecto se puede descargar el branch develop o master y posteriormente sincronizarlo, despues de esto se puede correr en cuaquier dispositivo, el keystore se encuentra en una carpeta llamada keystore, los password son 123456 y el alias es ComicVine.


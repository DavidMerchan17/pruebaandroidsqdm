package co.sqdm.comicvine.modules.profile;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.sqdm.comicvine.BuildConfig;
import co.sqdm.comicvine.R;
import co.sqdm.comicvine.Utilities.ManagerTakeImage;
import co.sqdm.comicvine.Utilities.StringUtilities;
import co.sqdm.comicvine.constants.Constants;
import co.sqdm.comicvine.database.entities.User;
import co.sqdm.comicvine.database.viewmodels.ProfileViewModel;
import co.sqdm.comicvine.modules.signup.SignUpActivity;

public class ProfileFragment extends Fragment {

    private OnFragmentInteractionListener listener;

    private ProfileViewModel viewModel;

    @BindView(R.id.profile_photo)
    ImageButton profilePhoto;

    @BindView(R.id.profile_name)
    EditText profileName;

    @BindView(R.id.profile_mail)
    EditText profileMail;

    @BindView(R.id.update_button)
    Button updateButton;

    @BindView(R.id.password_button)
    Button passwordButton;

    @BindView(R.id.close_change_password)
    ImageButton closeChangePassword;

    @BindView(R.id.profile_old_password)
    EditText profileOldPassword;

    @BindView(R.id.profile_new_password)
    EditText profileNewPassword;

    @BindView(R.id.profile_renew_password)
    EditText profileRenewPassword;

    @BindView(R.id.change_password)
    Button changePassword;

    @BindView(R.id.button_signout)
    Button buttonSignout;

    @BindView(R.id.view_change_password)
    RelativeLayout viewChangePassword;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewModel.getUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                viewModel.setUser(user);
                loadUserData();
            }
        });
    }

    private void loadUserData(){

        if (viewModel.user != null){
            Picasso.get().load("file://"+viewModel.getPhoto().getPath()).placeholder(R.drawable.ic_profile_black_24dp).into(profilePhoto);

            profileName.setText(viewModel.user.getFullname());
            profileMail.setText(viewModel.user.getMail());
        }

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUserInfo();
            }
        });

        passwordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewChangePassword.setVisibility(View.VISIBLE);
            }
        });

        profilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        closeChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewChangePassword.setVisibility(View.GONE);
            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePassword();
            }
        });

        buttonSignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signOut();
            }
        });
    }

    private void changePassword(){
        String oldPassword = profileOldPassword.getText().toString();
        String newPassword = profileNewPassword.getText().toString();
        String renewPassword = profileRenewPassword.getText().toString();

        if (oldPassword.equals(viewModel.user.getPassword()) && StringUtilities.validatePassword(newPassword) && newPassword.equals(renewPassword)){
            viewModel.updatePassword(newPassword);
            viewChangePassword.setVisibility(View.GONE);
            Toast.makeText(getContext(), R.string.update_success, Toast.LENGTH_LONG).show();
        }
    }

    private void signOut(){
        AlertDialog.Builder alertSignOut = new AlertDialog.Builder(getContext());
        alertSignOut.setMessage(R.string.signout_message);
        alertSignOut.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                viewModel.signOut();
                listener.onFragmentInteraction();

            }
        });
        alertSignOut.setNegativeButton(R.string.cancel, null);
        alertSignOut.show();
    }

    private void checkPermissions(){
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(), Constants.PERMISSIONS, Constants.REQUEST_PERMISSIONS);
        }else {
            selectPhoto();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.REQUEST_PERMISSIONS){
            selectPhoto();
        }
    }

    private void updateUserInfo(){
        String name = profileName.getText().toString();
        String mail = profileMail.getText().toString();

        if (name != null && !name.isEmpty() && StringUtilities.validateMail(mail)){
            viewModel.updateUser(name, mail);
            Toast.makeText(getContext(), R.string.update_success, Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(getContext(), R.string.message_info_error, Toast.LENGTH_LONG).show();
        }
    }

    private void selectPhoto(){
        AlertDialog.Builder alertDialogOptions = new AlertDialog.Builder(getContext());
        alertDialogOptions.setTitle(getString(R.string.take_image)).setItems(R.array.options_camera, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int option) {
                switch (option){
                    case 0:
                        startActivityForResult(ManagerTakeImage.openCameraIntent(getContext()), Constants.INTENT_CAMERA);
                        break;
                    case 1:
                        startActivityForResult(ManagerTakeImage.openGaleryIntent(), Constants.INTENT_GALERY);
                        break;
                }
            }
        });
        alertDialogOptions.show();
    }

    private void setUsePhoto(Uri imageUri){
        String imagePath = StringUtilities.getRealmPathImage(getContext(), imageUri);
        if (imagePath != null && !imagePath.isEmpty()){
            viewModel.setPhoto(imagePath);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener){
            listener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == Constants.INTENT_CAMERA){
                Uri imageUri = ManagerTakeImage.getFileUri();
                Picasso.get().load(imageUri).placeholder(R.drawable.ic_profile_black_24dp).into(profilePhoto);
                setUsePhoto(imageUri);
            }else if (requestCode == Constants.INTENT_GALERY){
                Uri imageUri = ManagerTakeImage.getPickImageResultUri(data);
                Picasso.get().load(imageUri).placeholder(R.drawable.ic_profile_black_24dp).into(profilePhoto);
                setUsePhoto(imageUri);
            }
        }
    }
}

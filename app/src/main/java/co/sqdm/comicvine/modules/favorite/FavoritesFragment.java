package co.sqdm.comicvine.modules.favorite;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.sqdm.comicvine.R;
import co.sqdm.comicvine.database.ImplementUser;
import co.sqdm.comicvine.database.entities.ComicMovie;
import co.sqdm.comicvine.database.viewmodels.FavoritesViewModel;
import co.sqdm.comicvine.modules.favorite.adapter.AdapterFavorites;
import co.sqdm.comicvine.modules.home.HomeFragment;
import co.sqdm.comicvine.modules.home.adapter.AdapterComicList;
import co.sqdm.comicvine.modules.home.adapter.OnItemClickListener;

public class FavoritesFragment extends Fragment implements OnItemClickListener {

    private OnFragmentInteractionListener listener;
    private FavoritesViewModel viewModel;
    private AdapterFavorites adapter;

    @BindView(R.id.favorites_comics_list)
    RecyclerView recyclerView;

    @BindView(R.id.favorite_empty_view)
    RelativeLayout favoriteEmptyView;

    public static FavoritesFragment newInstance() {
        FavoritesFragment fragment = new FavoritesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(FavoritesViewModel.class);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        adapter = new AdapterFavorites(viewModel.comicMovies, this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(null);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if (ImplementUser.getInstance().getUser().getFavorites_movies() != null && !ImplementUser.getInstance().getUser().getFavorites_movies().isEmpty()){
            viewModel.getFavoriteMovies().observe(this, new Observer<List<ComicMovie>>() {
                @Override
                public void onChanged(@Nullable List<ComicMovie> comicMovies) {
                    viewModel.comicMovies.clear();
                    viewModel.comicMovies.addAll(comicMovies);
                    adapter.notifyDataSetChanged();
                }
            });
        }else {
            favoriteEmptyView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HomeFragment.OnFragmentInteractionListener){
            listener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onItemClickListener(ComicMovie item) {
        listener.onFragmentInteraction(String.valueOf(item.getId()));
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String movieID);
    }
}

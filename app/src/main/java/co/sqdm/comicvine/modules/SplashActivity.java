package co.sqdm.comicvine.modules;

import android.Manifest;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import co.sqdm.comicvine.R;
import co.sqdm.comicvine.api.ApiClient;
import co.sqdm.comicvine.api.ApiClientImpl;
import co.sqdm.comicvine.database.daos.ComicMovieDao;
import co.sqdm.comicvine.database.entities.ComicMovie;
import co.sqdm.comicvine.database.room_db.ComicRoomDB;
import co.sqdm.comicvine.database.viewmodels.SplasScreenViewModel;
import co.sqdm.comicvine.modules.navigation_main.MainActivity;
import rx.Subscriber;

public class SplashActivity extends AppCompatActivity {

    private SplasScreenViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        viewModel = ViewModelProviders.of(this).get(SplasScreenViewModel.class);
        viewModel.init();

        getComicList();

    }

    private void getComicList(){
        viewModel.getComicMovies().observe(this, new Observer<List<ComicMovie>>() {
            @Override
            public void onChanged(@Nullable List<ComicMovie> comicMovies) {
                if (!comicMovies.isEmpty()){
                    openActivity();
                }
            }
        });
    }

    private void openActivity(){
        Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
    }

}

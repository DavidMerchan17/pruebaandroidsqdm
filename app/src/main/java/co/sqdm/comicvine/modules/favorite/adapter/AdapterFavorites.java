package co.sqdm.comicvine.modules.favorite.adapter;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.sqdm.comicvine.R;
import co.sqdm.comicvine.database.ImplementUser;
import co.sqdm.comicvine.database.entities.ComicMovie;
import co.sqdm.comicvine.database.entities.User;
import co.sqdm.comicvine.modules.home.adapter.AdapterComicList;
import co.sqdm.comicvine.modules.home.adapter.OnItemClickListener;

public class AdapterFavorites extends RecyclerView.Adapter<AdapterFavorites.ViewHolder> {

    private OnItemClickListener clickListener;
    private List<ComicMovie> items;
    private User user;

    public AdapterFavorites(List<ComicMovie> items, OnItemClickListener clickListener){
        this.items = items;
        this.clickListener = clickListener;
        this.user = ImplementUser.getInstance().getUser();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_favorites, parent, false);
        return new AdapterFavorites.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ComicMovie item = items.get(position);
        String imagePath = item.getImage().getMediumUrl();

        Picasso.get().load(imagePath).placeholder(R.drawable.ic_camera_roll_black_24dp).into(holder.movieImage);
        holder.nameMovieTextView.setText(item.getName());
        holder.movieDetail.setText(item.getDeck());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onItemClickListener(item);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.favorite_image)
        ImageView movieImage;

        @BindView(R.id.favorite_name)
        TextView nameMovieTextView;

        @BindView(R.id.favorite_detail)
        TextView movieDetail;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}

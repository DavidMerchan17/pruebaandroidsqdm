package co.sqdm.comicvine.modules.home;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.sqdm.comicvine.R;
import co.sqdm.comicvine.database.entities.ComicMovie;
import co.sqdm.comicvine.database.viewmodels.HomeViewModel;
import co.sqdm.comicvine.modules.home.adapter.AdapterComicList;
import co.sqdm.comicvine.modules.home.adapter.OnItemClickListener;

public class HomeFragment extends Fragment implements OnItemClickListener {

    private OnFragmentInteractionListener fragmentListener;
    private HomeViewModel homeViewModel;
    private AdapterComicList adapter;
    private List<ComicMovie> comicMovieList;

    @BindView(R.id.home_comics_list)
    RecyclerView home_list;

    @BindView(R.id.home_empty_view)
    RelativeLayout empty_view;

    @BindView(R.id.home_loading_view)
    RelativeLayout loading_view;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new AdapterComicList(homeViewModel.comicMovieLists, this);

        StaggeredGridLayoutManager stagger = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        home_list.setLayoutManager(stagger);
        home_list.setItemAnimator(null);
        home_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if (homeViewModel.comicMovieLists.isEmpty()){
            loading_view.setVisibility(View.VISIBLE);
            homeViewModel.getComicMovies().observe(this, new Observer<List<ComicMovie>>() {
                @Override
                public void onChanged(@Nullable List<ComicMovie> comicMovies) {
                    loading_view.setVisibility(View.GONE);

                    homeViewModel.comicMovieLists.clear();
                    homeViewModel.comicMovieLists.addAll(comicMovies);
                    adapter.notifyDataSetChanged();

                    if (comicMovies.isEmpty()){
                        empty_view.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener){
            fragmentListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String movieID);
    }

    @Override
    public void onItemClickListener(ComicMovie item) {
        if (fragmentListener != null){
            fragmentListener.onFragmentInteraction(String.valueOf(item.getId()));
        }
    }
}

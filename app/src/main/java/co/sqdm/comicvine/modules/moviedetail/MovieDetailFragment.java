package co.sqdm.comicvine.modules.moviedetail;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.sqdm.comicvine.R;
import co.sqdm.comicvine.database.entities.ComicMovie;
import co.sqdm.comicvine.database.entities.Producer;
import co.sqdm.comicvine.database.entities.Studio;
import co.sqdm.comicvine.database.entities.Writer;
import co.sqdm.comicvine.database.viewmodels.MovieDetailViewModel;

public class MovieDetailFragment extends Fragment {

    private static final String ARG_MOVIE_ID = "movieID";
    private String movieID;
    private MovieDetailViewModel viewModel;

    @BindView(R.id.detail_image)
    ImageView image;

    @BindView(R.id.detail_name)
    TextView name;

    @BindView(R.id.detail_movie)
    WebView detail;

    @BindView(R.id.detail_release_date)
    TextView releaseDate;

    @BindView(R.id.detail_rating)
    TextView rating;

    @BindView(R.id.button_add_favorite)
    ImageButton buttonAddFavorite;

    public static MovieDetailFragment newInstance(String movieID) {
        MovieDetailFragment fragment = new MovieDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_MOVIE_ID, movieID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(MovieDetailViewModel.class);
        viewModel.init();

        if (getArguments() != null && viewModel.movie == null) {
            movieID = getArguments().getString(ARG_MOVIE_ID);
            loadMovieData();
        }else{
            completeFields();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        buttonAddFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isMovieFavorite()){
                    viewModel.removeMovieUser();
                    buttonAddFavorite.setImageResource(R.drawable.ic_star_empty_black_24dp);
                }else {
                    viewModel.addMovieUser();
                    buttonAddFavorite.setImageResource(R.drawable.ic_star_gold_24dp);
                }
            }
        });
    }

    private void loadMovieData(){

        viewModel.getMovie(movieID).observe(this, new Observer<ComicMovie>() {
            @Override
            public void onChanged(@Nullable ComicMovie comicMovie) {
                viewModel.movie = comicMovie;
                completeFields();
            }
        });
    }

    private void completeFields(){

        Picasso.get().load(viewModel.movie.getImage().getMediumUrl()).placeholder(R.drawable.ic_camera_roll_black_24dp).into(image);

        name.setText(viewModel.movie.getName());
        releaseDate.setText(viewModel.movie.getReleaseDate());
        rating.setText(viewModel.movie.getRating());

        if (isMovieFavorite()){
            buttonAddFavorite.setImageResource(R.drawable.ic_star_gold_24dp);
        }

        configureDetail();
    }

    private boolean isMovieFavorite(){
        return (viewModel.movie != null && viewModel.user != null && viewModel.user.getFavorites_movies() != null && !viewModel.user.getFavorites_movies().isEmpty() && viewModel.user.getFavorites_movies().contains(String.valueOf(viewModel.movie.getId())));
    }

    private void configureDetail(){
        String detailString = "";
        if (viewModel.movie.getDescription() == null || viewModel.movie.getDescription().isEmpty()){
            detailString = viewModel.movie.getDeck();
        }else {
            detailString = viewModel.movie.getDescription();
        }

        if (viewModel.movie.getWriters() != null && !viewModel.movie.getWriters().isEmpty()){
            detailString = detailString + "<h2>" + getContext().getString(R.string.writers) + "</h2><ul>" ;

            for (Writer writer : viewModel.movie.getWriters()){
                detailString = detailString + "<li>"+writer.getName()+"</li>";
            }
        }

        if (viewModel.movie.getProducers() != null && !viewModel.movie.getProducers().isEmpty()){
            detailString = detailString + "</ul><h2>" + getContext().getString(R.string.producers) + "</h2><ul>" ;

            for (Producer producer : viewModel.movie.getProducers()){
                detailString = detailString + "<li>"+producer.getName()+"</li>";
            }
        }

        if (viewModel.movie.getStudios() != null && !viewModel.movie.getStudios().isEmpty()){
            detailString = detailString + "</ul><h2>" + getContext().getString(R.string.studios) + "</h2><ul>" ;

            for (Studio studio : viewModel.movie.getStudios()){
                detailString = detailString + "<li>"+studio.getName()+"</li>";
            }
        }

        detailString = detailString + "</ul>" ;

        detail.loadData(detailString,"text/html","utf-8");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_detail, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

}

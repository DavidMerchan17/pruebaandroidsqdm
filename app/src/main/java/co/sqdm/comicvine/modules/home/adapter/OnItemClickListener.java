package co.sqdm.comicvine.modules.home.adapter;

import co.sqdm.comicvine.database.entities.ComicMovie;

public interface OnItemClickListener {
    void onItemClickListener(ComicMovie item);
}

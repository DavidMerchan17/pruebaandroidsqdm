package co.sqdm.comicvine.modules.signup;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.sqdm.comicvine.R;
import co.sqdm.comicvine.Utilities.ManagerTakeImage;
import co.sqdm.comicvine.Utilities.StringUtilities;
import co.sqdm.comicvine.constants.Constants;
import co.sqdm.comicvine.database.entities.User;
import co.sqdm.comicvine.database.room_db.ComicRoomDB;
import co.sqdm.comicvine.database.viewmodels.SignUpViewModel;
import co.sqdm.comicvine.modules.navigation_main.MainActivity;

public class SignUpActivity extends AppCompatActivity {

    private SignUpViewModel viewModel;

    @BindView(R.id.toolbar_signup)
    Toolbar toolbar;

    @BindView(R.id.signup_photo)
    ImageButton photo;

    @BindView(R.id.signup_name)
    EditText name;

    @BindView(R.id.signup_mail)
    EditText mail;

    @BindView(R.id.signup_password)
    EditText password;

    @BindView(R.id.signup_button)
    Button signUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.title_signup);

        viewModel = ViewModelProviders.of(this).get(SignUpViewModel.class);
        viewModel.init();

        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveUser();
            }
        });
    }

    private void checkPermissions(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, Constants.PERMISSIONS, Constants.REQUEST_PERMISSIONS);
        }else {
            selectPhoto();
        }
    }

    private void selectPhoto(){
        AlertDialog.Builder alertDialogOptions = new AlertDialog.Builder(this);
        alertDialogOptions.setTitle(getString(R.string.take_image)).setItems(R.array.options_camera, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int option) {
                switch (option){
                    case 0:
                        startActivityForResult(ManagerTakeImage.openCameraIntent(SignUpActivity.this), Constants.INTENT_CAMERA);
                        break;
                    case 1:
                        startActivityForResult(ManagerTakeImage.openGaleryIntent(), Constants.INTENT_GALERY);
                        break;
                }
            }
        });
        alertDialogOptions.show();
    }

    private void saveUser(){
        String userName = name.getText().toString();
        final String userMail = mail.getText().toString();
        String userPassword = password.getText().toString();

        if (!userName.isEmpty() && StringUtilities.validateMail(userMail) && StringUtilities.validatePassword(userPassword)){
            viewModel.saveUserDB(userName, userMail, userPassword);
            viewModel.getUser().observe(this, new Observer<User>() {
                @Override
                public void onChanged(@Nullable User user) {
                    if (user != null && user.getId() == viewModel.user.getId()){
                        userSaveSuccess();
                    }
                }
            });
        }else {
            Toast.makeText(this,R.string.message_info_error,Toast.LENGTH_LONG).show();
        }
    }

    private void userSaveSuccess(){

        name.setText("");
        mail.setText("");
        password.setText("");

        AlertDialog.Builder alertSuccess = new AlertDialog.Builder(this);
        alertSuccess.setMessage(R.string.alert_save_success_message);
        alertSuccess.setPositiveButton(R.string.alert_save_success_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                loginUser();
            }
        });
        alertSuccess.setNegativeButton(R.string.alert_save_success_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onBackPressed();
            }
        });
        alertSuccess.show();
    }

    private void setUsePhoto(Uri imageUri){
        String imagePath = StringUtilities.getRealmPathImage(SignUpActivity.this, imageUri);
        if (imagePath != null && !imagePath.isEmpty()){
            viewModel.setPhoto(imagePath);
        }
    }

    private void loginUser(){
        viewModel.loginUser();
        Intent mainIntent = new Intent(SignUpActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainIntent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.REQUEST_PERMISSIONS){
            selectPhoto();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == Constants.INTENT_CAMERA){
                Uri imageUri = ManagerTakeImage.getFileUri();
                Picasso.get().load(imageUri).placeholder(R.drawable.ic_profile_black_24dp).into(photo);
                setUsePhoto(imageUri);
            }else if (requestCode == Constants.INTENT_GALERY){
                Uri imageUri = ManagerTakeImage.getPickImageResultUri(data);
                Picasso.get().load(imageUri).placeholder(R.drawable.ic_profile_black_24dp).into(photo);
                setUsePhoto(imageUri);
            }
        }
    }

}

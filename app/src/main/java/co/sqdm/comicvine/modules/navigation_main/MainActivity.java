package co.sqdm.comicvine.modules.navigation_main;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.common.collect.Lists;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.sqdm.comicvine.R;
import co.sqdm.comicvine.Utilities.StringUtilities;
import co.sqdm.comicvine.database.ImplementUser;
import co.sqdm.comicvine.database.entities.User;
import co.sqdm.comicvine.database.viewmodels.MainViewModel;
import co.sqdm.comicvine.modules.favorite.FavoritesFragment;
import co.sqdm.comicvine.modules.home.HomeFragment;
import co.sqdm.comicvine.modules.moviedetail.MovieDetailFragment;
import co.sqdm.comicvine.modules.profile.ProfileFragment;
import co.sqdm.comicvine.modules.signup.SignUpActivity;


public class MainActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener,FavoritesFragment.OnFragmentInteractionListener, ProfileFragment.OnFragmentInteractionListener {

    private boolean isUserLogged = false;
    private List<Fragment> fragments;
    private MainViewModel viewModel;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    @BindView(R.id.login_container)
    LinearLayout loginContainer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.login_button)
    Button loginButton;

    @BindView(R.id.signup_button)
    Button signUpButton;

    @BindView(R.id.login_view)
    RelativeLayout loginView;

    @BindView(R.id.close_login_view)
    ImageButton closeLoginView;

    @BindView(R.id.mail_login_view)
    EditText mailLoginView;

    @BindView(R.id.password_login_view)
    EditText passwordLoginView;

    @BindView(R.id.button_login_view)
    Button buttonLoginView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.title_home);
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        fragments = Lists.newArrayList();

        if (viewModel.getCurrentFragment() == null){
            fragmentExecutor(HomeFragment.newInstance(), R.id.fragment_container,false);
        }
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginView.setVisibility(View.VISIBLE);
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSignUp();
            }
        });

        closeLoginView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginView.setVisibility(View.GONE);
            }
        });

        buttonLoginView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginUser();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.getUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                viewModel.setUser(user);
                validateUser();
            }
        });
    }

    private void validateUser(){
        if (viewModel.user != null){
            loginContainer.setVisibility(View.GONE);
            navigation.setVisibility(View.VISIBLE);
            isUserLogged = true;
        }else {
            loginContainer.setVisibility(View.VISIBLE);
            navigation.setVisibility(View.GONE);
            isUserLogged = false;
        }
    }

    private void loginUser(){
        String mail = mailLoginView.getText().toString();
        String password = passwordLoginView.getText().toString();

        if (StringUtilities.validateMail(mail) && StringUtilities.validatePassword(password)){
            viewModel.getLoginUser(mail, password);
            loginView.setVisibility(View.GONE);
            validateUser();
        }else {
            Toast.makeText(this, R.string.message_info_error,Toast.LENGTH_LONG).show();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    toolbar.setTitle(R.string.title_home);
                    fragmentExecutor(HomeFragment.newInstance(), R.id.fragment_container,false);
                    return true;
                case R.id.navigation_favorite:
                    toolbar.setTitle(R.string.title_favorite);
                    fragmentExecutor(FavoritesFragment.newInstance(), R.id.fragment_container,false);
                    return true;
                case R.id.navigation_profile:
                    toolbar.setTitle(R.string.title_profile);
                    fragmentExecutor(ProfileFragment.newInstance(), R.id.fragment_container,false);
                    return true;
            }
            return false;
        }
    };

    public void fragmentExecutor(Fragment fragment, int containerId, boolean push){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment oldFragment = getPreviouslyFragment();
        if (fragment != null) {
            if (!fragment.isAdded() && !fragments.contains(fragment)){
                fragments.add(fragment);
                oldFragment = getPreviouslyFragment();
                if (oldFragment != null){
                    fragmentTransaction.remove(oldFragment);
                    fragmentTransaction.replace(containerId, fragment);
                }else {
                    fragmentTransaction.add(containerId, fragment);
                }

                if (push){
                    fragmentTransaction.addToBackStack(fragment.getTag());
                }
                viewModel.setCurrentFragment(fragment);
                fragmentTransaction.commitAllowingStateLoss();
            }else if (oldFragment != null) {
                fragmentTransaction.remove(oldFragment);
                fragmentTransaction.replace(containerId, fragment);
                if (push){
                    fragmentTransaction.addToBackStack(fragment.getTag());
                }
                fragmentTransaction.commitAllowingStateLoss();
            }
        }

    }

    private Fragment getPreviouslyFragment(){
        return fragments.size() > 1 ? fragments.get(fragments.size() - 2): null;
    }

    private void showAlertLoggin(){
        AlertDialog.Builder alertLogin = new AlertDialog.Builder(this);
        alertLogin.setMessage(R.string.alert_login_message);
        alertLogin.setPositiveButton(R.string.alert_login_message_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                openSignUp();
            }
        });
        alertLogin.setNegativeButton(R.string.alert_login_message_cancel, null);
        alertLogin.show();
    }

    private void openSignUp(){
        Intent signUp = new Intent(MainActivity.this, SignUpActivity.class);
        startActivity(signUp);
    }

    @Override
    public void onFragmentInteraction(String movieID) {
        if (isUserLogged){
            fragmentExecutor(MovieDetailFragment.newInstance(movieID), R.id.fragment_container,true);
        }else {
            showAlertLoggin();
        }
    }

    @Override
    public void onFragmentInteraction() {
        fragmentExecutor(HomeFragment.newInstance(), R.id.fragment_container,false);
    }
}

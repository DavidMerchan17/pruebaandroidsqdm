package co.sqdm.comicvine.constants;


import android.Manifest;

public class Constants {

    public static Constants instance;
    public static String serverUrl = "https://comicvine.gamespot.com";
    public static String apiKey = "63cefca9edbe4d39acea205671c0c6865f0726bd";
    public static String nameDB = "ComicVineDB";
    public static String directoryName = "ComicVine";
    public static int REQUEST_PERMISSIONS = 0x244;
    public static int INTENT_CAMERA = 0x245;
    public static int INTENT_GALERY = 0x246;
    public static String[] PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

}

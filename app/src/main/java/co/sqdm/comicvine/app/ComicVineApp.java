package co.sqdm.comicvine.app;

import android.app.Application;
import android.content.Context;

public class ComicVineApp extends Application {

    private static Application application;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        application = this;
    }

    public static Context getContext() {
        return context;
    }

    public static Application getInstance(){
        return application;
    }

}

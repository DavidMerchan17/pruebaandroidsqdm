package co.sqdm.comicvine.Utilities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.google.common.collect.Lists;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import co.sqdm.comicvine.BuildConfig;
import co.sqdm.comicvine.constants.Constants;

public class ManagerTakeImage {
    private static String TAG = "TakePicture";
    private static Uri fileUri;

    public static Uri getFileUri() {
        return fileUri;
    }

    public static void setFileUri(Uri fileUri) {
        ManagerTakeImage.fileUri = fileUri;
    }

    public static Intent openCameraIntent(Context context){
        Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", getCaptureImageOutputUri());
        setFileUri(uri);

        Intent captureIntent = new  Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getFileUri());
        return captureIntent;
    }

    public static Intent openGaleryIntent(){
        setFileUri(null);
        Intent galleryIntent = new  Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        galleryIntent.putExtra(MediaStore.EXTRA_OUTPUT, getFileUri());
        return galleryIntent;
    }

    public static File getCaptureImageOutputUri() {

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Constants.directoryName);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("ERROR", "Error al crear el directorio");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

        return new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
    }

    public static Uri getPickImageResultUri(Intent  data) {

        boolean isCamera = true;

        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null  && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }else{
            Log.i(TAG, "Data is null");
        }

        ManagerTakeImage.fileUri = isCamera ?  fileUri : data.getData();

        return ManagerTakeImage.fileUri;
    }

}

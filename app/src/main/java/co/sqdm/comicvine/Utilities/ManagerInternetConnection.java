package co.sqdm.comicvine.Utilities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityCompat;

import co.sqdm.comicvine.app.ComicVineApp;
import co.sqdm.comicvine.modules.navigation_main.MainActivity;

public class ManagerInternetConnection {

    private static ManagerInternetConnection instance;

    public static ManagerInternetConnection getInstance(){
        if (instance == null){
            instance = new ManagerInternetConnection();
        }
        return instance;
    }

    public boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager) ComicVineApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

}

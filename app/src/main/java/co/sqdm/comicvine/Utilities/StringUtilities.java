package co.sqdm.comicvine.Utilities;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Patterns;

import java.util.regex.Pattern;

public class StringUtilities {

    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=\\S+$)" +           //no white spaces
                    ".{4,}" +               //at least 4 characters
                    "$");

    public static String getRealmPathImage(Context context, Uri uri){
        Cursor cursor = null;
        try {
            String[] data = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(uri,  data, null, null, null);
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return "file://"+cursor.getString(columnIndex);
        }catch (Exception e){
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            return "file://"+uri.getPath();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static boolean validatePassword(String password) {
        String passwordInput = password.trim();

        if (passwordInput.isEmpty()) {
            return false;
        } else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean validateMail(String mail) {
        String emailInput = mail.trim();

        if (emailInput.isEmpty()) {
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            return false;
        } else {
            return true;
        }
    }
}

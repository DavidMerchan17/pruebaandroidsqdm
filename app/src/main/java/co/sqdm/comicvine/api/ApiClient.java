package co.sqdm.comicvine.api;

import android.arch.lifecycle.LiveData;

import java.util.List;

import co.sqdm.comicvine.database.entities.ComicMovie;

public interface ApiClient {
    LiveData<List<ComicMovie>> getMovies();
}

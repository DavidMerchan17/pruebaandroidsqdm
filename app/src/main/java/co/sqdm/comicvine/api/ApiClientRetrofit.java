package co.sqdm.comicvine.api;

import co.sqdm.comicvine.database.entities.ResponseApi;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ApiClientRetrofit {
    @Headers("Content-Type:application/json")
    @GET("/api/movies/?format=json")
    Call<ResponseApi> getMovies(@Query("api_key") String apiKey);
}

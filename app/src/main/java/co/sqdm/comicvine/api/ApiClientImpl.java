package co.sqdm.comicvine.api;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import co.sqdm.comicvine.R;
import co.sqdm.comicvine.Utilities.ManagerInternetConnection;
import co.sqdm.comicvine.constants.Constants;
import co.sqdm.comicvine.database.daos.ComicMovieDao;
import co.sqdm.comicvine.database.entities.ComicMovie;
import co.sqdm.comicvine.database.entities.ResponseApi;
import co.sqdm.comicvine.database.room_db.ComicRoomDB;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;

public class ApiClientImpl implements ApiClient {

    private final ApiClientRetrofit apiClientRetrofit;

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.serverUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public ApiClientImpl(){
        apiClientRetrofit = retrofit.create(ApiClientRetrofit.class);
    }

    @Override
    public LiveData<List<ComicMovie>> getMovies() {
        final MutableLiveData<List<ComicMovie>> data = new MutableLiveData<>();

        Call<ResponseApi> call = apiClientRetrofit.getMovies(Constants.apiKey);
        call.enqueue(new Callback<ResponseApi>() {
            @Override
            public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                List<ComicMovie> comicMovieList = response.body().getResults();
                data.setValue(comicMovieList);
                ComicRoomDB.getInstance().comicMovieDao().insertAll(comicMovieList);
            }

            @Override
            public void onFailure(Call<ResponseApi> call, Throwable t) {

            }
        });

        if (ManagerInternetConnection.getInstance().isConnected()) {

        } else {

        }

        return data;
    }

}

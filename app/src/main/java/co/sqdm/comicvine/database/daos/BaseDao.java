package co.sqdm.comicvine.database.daos;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

import java.util.List;

public interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(T... entityToInsert);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<T> entitiesToInsert);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(T[] entitiesToInsert);

    @Update
    void update(T... entityToInsert);

    @Update
    void updateAll(List<T> entitiesToInsert);

    @Update
    void updateAll(T[] entitiesToInsert);

    @Delete
    void delete(T entityToDelete);

    @Delete
    void delete(List<T> entitiesToDelete);

    @Delete
    void delete(T[] entitiesToDelete);

}

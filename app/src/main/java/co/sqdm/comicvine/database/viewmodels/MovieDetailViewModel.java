package co.sqdm.comicvine.database.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.google.common.collect.Lists;

import java.util.List;

import co.sqdm.comicvine.database.ImplementUser;
import co.sqdm.comicvine.database.entities.ComicMovie;
import co.sqdm.comicvine.database.entities.User;
import co.sqdm.comicvine.database.room_db.ComicRoomDB;

public class MovieDetailViewModel extends ViewModel {
    public ComicMovie movie;
    public User user;

    public void init(){
        if (user == null)
            user = ImplementUser.getInstance().getUser();
    }

    public LiveData<ComicMovie> getMovie(String movieId){
        return ComicRoomDB.getInstance().comicMovieDao().getComicMovieFromId(movieId);
    }

    public void addMovieUser(){

        List<String> movieList = Lists.newArrayList();
        if (user.getFavorites_movies() == null || user.getFavorites_movies().isEmpty()){
            user.setFavorites_movies(movieList);
        }else {
            movieList.addAll(user.getFavorites_movies());
        }
        movieList.add(String.valueOf(movie.getId()));
        user.setFavorites_movies(movieList);

        ComicRoomDB.getInstance().userDao().update(user);

    }

    public void removeMovieUser(){

        List<String> movieList = Lists.newArrayList();
        if (user.getFavorites_movies() == null || user.getFavorites_movies().isEmpty()){
            user.setFavorites_movies(movieList);
        }else {
            movieList.addAll(user.getFavorites_movies());
        }

        if (movieList.contains(String.valueOf(movie.getId()))){
            movieList.remove(String.valueOf(movie.getId()));
        }

        user.setFavorites_movies(movieList);
        ComicRoomDB.getInstance().userDao().update(user);
    }


}

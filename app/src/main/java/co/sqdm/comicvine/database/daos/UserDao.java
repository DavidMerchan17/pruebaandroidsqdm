package co.sqdm.comicvine.database.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import co.sqdm.comicvine.database.entities.User;

@Dao
public interface UserDao extends BaseDao<User> {

    @Query("SELECT * FROM User ")
    List<User> getAllUsers();

    @Query("SELECT * FROM User WHERE id = :id ")
    LiveData<User> getUserFromId(int id);

    @Query("SELECT * FROM User WHERE isLogin = 1 ")
    User getUserLogin();

    @Query("SELECT * FROM User WHERE isLogin = 1 ")
    LiveData<User> getUserLoginAsyng();

    @Query("SELECT * FROM User WHERE mail = :mail AND password = :password ")
    User getUserFromLogin(String mail, String password);

    @Query("SELECT count(*) FROM User")
    Integer getCountUser();

}

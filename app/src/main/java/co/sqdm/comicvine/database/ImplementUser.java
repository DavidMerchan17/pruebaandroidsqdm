package co.sqdm.comicvine.database;

import com.google.common.collect.Lists;

import java.util.List;

import co.sqdm.comicvine.database.daos.UserDao;
import co.sqdm.comicvine.database.entities.User;
import co.sqdm.comicvine.database.room_db.ComicRoomDB;

public class ImplementUser {

    private static ImplementUser instance;
    private final UserDao userDao;
    private User user;

    private ImplementUser(){
        userDao = ComicRoomDB.getInstance().userDao();
    }

    public static ImplementUser getInstance(){
        if (instance == null)
            instance = new ImplementUser();

        return instance;
    }

    public User getUser(){
        if (user == null)
            user = userDao.getUserLogin();

        return user;
    }

    public void updateUser(User user){
        userDao.update(user);
    }

    public void signOutUser(){
        List<User> users = userDao.getAllUsers();
        for (User user : users){
            user.setLogin(false);
            userDao.update(user);
        }
    }

    public void addMovieFavorite(String movieID){
        User user = getUser();
        List<String> moviesID = Lists.newArrayList();
        if (user.getFavorites_movies() != null && !user.getFavorites_movies().isEmpty()){
            moviesID.addAll(user.getFavorites_movies());
        }
        moviesID.add(movieID);
        user.setFavorites_movies(moviesID);
        updateUser(user);

    }

}

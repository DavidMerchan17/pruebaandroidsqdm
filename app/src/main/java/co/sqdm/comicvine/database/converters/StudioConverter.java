package co.sqdm.comicvine.database.converters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import co.sqdm.comicvine.database.entities.Studio;

public class StudioConverter {
    @TypeConverter
    public List<Studio> stringToGson(String object){
        Gson gson = new Gson();
        Type studio = new TypeToken<List<Studio>>(){}.getType();
        return gson.fromJson(object, studio);
    }

    @TypeConverter
    public String gsonToString(List<Studio> object){
        Gson gson = new Gson();
        return gson.toJson(object);
    }
}

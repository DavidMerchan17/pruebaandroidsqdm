package co.sqdm.comicvine.database.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import java.util.List;
import co.sqdm.comicvine.api.ApiClient;
import co.sqdm.comicvine.api.ApiClientImpl;
import co.sqdm.comicvine.database.entities.ComicMovie;
import co.sqdm.comicvine.database.room_db.ComicRoomDB;

public class SplasScreenViewModel extends ViewModel {

    private ApiClient apiClient;

    public void init(){
        apiClient = new ApiClientImpl();
        apiClient.getMovies();
    }

    public LiveData<List<ComicMovie>> getComicMovies() {
        return ComicRoomDB.getInstance().comicMovieDao().getAllComicMovie();
    }

}

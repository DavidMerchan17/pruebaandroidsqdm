package co.sqdm.comicvine.database.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.net.Uri;

import co.sqdm.comicvine.database.ImplementUser;
import co.sqdm.comicvine.database.entities.User;
import co.sqdm.comicvine.database.room_db.ComicRoomDB;

public class ProfileViewModel extends ViewModel {

    public User user;

    public LiveData<User> getUser(){
        return ComicRoomDB.getInstance().userDao().getUserLoginAsyng();
    }

    public void updateUser(String name, String mail){
        this.user.setFullname(name);
        this.user.setMail(mail);

        ComicRoomDB.getInstance().userDao().update(this.user);
    }

    public void updatePassword(String password){
        this.user.setPassword(password);
        ComicRoomDB.getInstance().userDao().update(this.user);
    }

    public void signOut(){
        ImplementUser.getInstance().signOutUser();
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Uri getPhoto(){
        return Uri.parse(user.getUrlPhoto());
    }

    public void setPhoto(String path){
        this.user.setUrlPhoto(path);
    }

}

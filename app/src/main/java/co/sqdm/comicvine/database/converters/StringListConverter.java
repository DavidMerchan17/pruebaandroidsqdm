package co.sqdm.comicvine.database.converters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import co.sqdm.comicvine.database.entities.Image;

public class StringListConverter {

    @TypeConverter
    public List<String> stringToGson(String object){
        Gson gson = new Gson();
        Type listString = new TypeToken<List<String>>(){}.getType();
        return gson.fromJson(object, listString);
    }

    @TypeConverter
    public String gsonToString(List<String> object){
        Gson gson = new Gson();
        return gson.toJson(object);
    }

}

package co.sqdm.comicvine.database.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.google.common.collect.Lists;

import java.util.List;
import co.sqdm.comicvine.database.entities.ComicMovie;
import co.sqdm.comicvine.database.room_db.ComicRoomDB;

public class HomeViewModel extends ViewModel {

    public List<ComicMovie> comicMovieLists = Lists.newArrayList();

    public LiveData<List<ComicMovie>> getComicMovies(){
        return ComicRoomDB.getInstance().comicMovieDao().getAllComicMovie();
    }

}

package co.sqdm.comicvine.database.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import co.sqdm.comicvine.database.entities.User;
import co.sqdm.comicvine.database.room_db.ComicRoomDB;

public class SignUpViewModel extends ViewModel {

    public User user;

    public void init(){
        if (user == null)
            user = new User();
    }

    public void saveUserDB(String name, String mail, String password){
        user.setId(ComicRoomDB.getInstance().userDao().getCountUser() + 1);
        user.setFullname(name);
        user.setMail(mail);
        user.setPassword(password);
        user.setLogin(false);
        ComicRoomDB.getInstance().userDao().insert(user);
    }

    public void loginUser(){
        user.setLogin(true);
        ComicRoomDB.getInstance().userDao().update(user);
    }

    public LiveData<User> getUser(){
        return ComicRoomDB.getInstance().userDao().getUserFromId(user.getId());
    }

    public void setPhoto(String photo){
        this.user.setUrlPhoto(photo);
    }
}

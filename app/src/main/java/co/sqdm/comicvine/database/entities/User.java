package co.sqdm.comicvine.database.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.sqdm.comicvine.database.converters.StringListConverter;
import co.sqdm.comicvine.database.converters.StudioConverter;

@Entity
public class User  {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("fullname")
    @Expose
    private String fullname;

    @SerializedName("url_photo")
    @Expose
    private String urlPhoto;

    @SerializedName("mail")
    @Expose
    private String mail;

    @SerializedName("password")
    @Expose
    private String password;

    @TypeConverters({StringListConverter.class})
    @SerializedName("favorites_movies")
    @Expose
    private List<String> favorites_movies;

    @SerializedName("is_login")
    @Expose
    private boolean isLogin;

    public User(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getFavorites_movies() {
        return favorites_movies;
    }

    public void setFavorites_movies(List<String> favorites_movies) {
        this.favorites_movies = favorites_movies;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }
}

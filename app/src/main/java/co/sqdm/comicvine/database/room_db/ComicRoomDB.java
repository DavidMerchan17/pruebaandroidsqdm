package co.sqdm.comicvine.database.room_db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import co.sqdm.comicvine.app.ComicVineApp;
import co.sqdm.comicvine.constants.Constants;
import co.sqdm.comicvine.database.daos.ComicMovieDao;
import co.sqdm.comicvine.database.daos.UserDao;
import co.sqdm.comicvine.database.entities.ComicMovie;
import co.sqdm.comicvine.database.entities.User;

@Database(entities = {ComicMovie.class,User.class}, version = 1, exportSchema = false)
public abstract class ComicRoomDB extends RoomDatabase {

    private static ComicRoomDB instance;
    public static ComicRoomDB getInstance(){
        if (instance == null || !instance.isOpen()){
            instance = Room.databaseBuilder(ComicVineApp.getInstance(), ComicRoomDB.class, Constants.nameDB).allowMainThreadQueries().build();
        }
        return instance;
    }

    public abstract ComicMovieDao comicMovieDao();

    public abstract UserDao userDao();
}

package co.sqdm.comicvine.database.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.google.common.collect.Lists;

import java.util.List;

import co.sqdm.comicvine.database.ImplementUser;
import co.sqdm.comicvine.database.entities.ComicMovie;
import co.sqdm.comicvine.database.entities.User;
import co.sqdm.comicvine.database.room_db.ComicRoomDB;

public class FavoritesViewModel extends ViewModel {

    public List<ComicMovie> comicMovies = Lists.newArrayList();

    public LiveData<List<ComicMovie>> getFavoriteMovies(){
        User user = ImplementUser.getInstance().getUser();
        return ComicRoomDB.getInstance().comicMovieDao().getComicMovieFromUser(user.getFavorites_movies());
    }

}

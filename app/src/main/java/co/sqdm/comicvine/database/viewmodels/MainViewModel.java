package co.sqdm.comicvine.database.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.v4.app.Fragment;

import co.sqdm.comicvine.database.ImplementUser;
import co.sqdm.comicvine.database.entities.User;
import co.sqdm.comicvine.database.room_db.ComicRoomDB;

public class MainViewModel extends ViewModel {

    public User user;
    private Fragment currentFragment;

    public LiveData<User> getUser(){
        return ComicRoomDB.getInstance().userDao().getUserLoginAsyng();
    }

    public void getLoginUser(String mail, String password){
        User user = ComicRoomDB.getInstance().userDao().getUserFromLogin(mail,password);
        user.setLogin(true);
        updateUser(user);
    }

    public void updateUser(User user) {
        this.user = user;
        this.user.setLogin(true);
        ImplementUser.getInstance().updateUser(this.user);
    }

    public Fragment getCurrentFragment() {
        return currentFragment;
    }

    public void setCurrentFragment(Fragment currentFragment) {
        this.currentFragment = currentFragment;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

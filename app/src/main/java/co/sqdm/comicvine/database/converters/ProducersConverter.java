package co.sqdm.comicvine.database.converters;

import android.arch.persistence.room.TypeConverter;

import co.sqdm.comicvine.database.entities.Producer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ProducersConverter {
    @TypeConverter
    public List<Producer> stringToGson(String object){
        Gson gson = new Gson();
        Type producer = new TypeToken<List<Producer>>(){}.getType();
        return gson.fromJson(object, producer);
    }

    @TypeConverter
    public String gsonToString(List<Producer> object){
        Gson gson = new Gson();
        return gson.toJson(object);
    }
}

package co.sqdm.comicvine.database.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import java.util.List;
import co.sqdm.comicvine.database.entities.ComicMovie;

@Dao
public interface ComicMovieDao extends BaseDao<ComicMovie> {

    @Query("SELECT * FROM ComicMovie")
    LiveData<List<ComicMovie>> getAllComicMovie();

    @Query("SELECT * FROM ComicMovie WHERE id = :comicMovieId ")
    LiveData<ComicMovie> getComicMovieFromId(String comicMovieId);

    @Query("SELECT * FROM ComicMovie WHERE id IN (:movies) ")
    LiveData<List<ComicMovie>> getComicMovieFromUser(List<String> movies);

}

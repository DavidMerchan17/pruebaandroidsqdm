package co.sqdm.comicvine.database.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import co.sqdm.comicvine.database.converters.ImageConverter;
import co.sqdm.comicvine.database.converters.ProducersConverter;
import co.sqdm.comicvine.database.converters.StudioConverter;
import co.sqdm.comicvine.database.converters.WriterConverter;

@Entity
public class ComicMovie {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("api_detail_url")
    @Expose
    private String apiDetailUrl;

    @SerializedName("box_office_revenue")
    @Expose
    private String boxOfficeRevenue;

    @SerializedName("budget")
    @Expose
    private String budget;

    @SerializedName("date_added")
    @Expose
    private String dateAdded;

    @SerializedName("date_last_updated")
    @Expose
    private String dateLastUpdated;

    @SerializedName("deck")
    @Expose
    private String deck;

    @SerializedName("description")
    @Expose
    private String description;

    @TypeConverters({ImageConverter.class})
    @SerializedName("image")
    @Expose
    private Image image;

    @SerializedName("name")
    @Expose
    private String name;

    @TypeConverters({ProducersConverter.class})
    @SerializedName("producers")
    @Expose
    private List<Producer> producers = null;

    @SerializedName("rating")
    @Expose
    private String rating;

    @SerializedName("release_date")
    @Expose
    private String releaseDate;

    @SerializedName("runtime")
    @Expose
    private String runtime;

    @SerializedName("site_detail_url")
    @Expose
    private String siteDetailUrl;

    @TypeConverters({StudioConverter.class})
    @SerializedName("studios")
    @Expose
    private List<Studio> studios = null;

    @SerializedName("total_revenue")
    @Expose
    private String totalRevenue;

    @TypeConverters({WriterConverter.class})
    @SerializedName("writers")
    @Expose
    private List<Writer> writers = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public ComicMovie() {
    }

    /**
     *
     * @param apiDetailUrl
     * @param budget
     * @param hasStaffReview
     * @param studios
     * @param totalRevenue
     * @param runtime
     * @param dateAdded
     * @param image
     * @param id
     * @param boxOfficeRevenue
     * @param siteDetailUrl
     * @param releaseDate
     * @param description
     * @param dateLastUpdated
     * @param name
     * @param writers
     * @param distributor
     * @param rating
     * @param deck
     * @param producers
     */
    public ComicMovie(String apiDetailUrl, String boxOfficeRevenue, String budget, String dateAdded, String dateLastUpdated, String deck, String description, Object distributor, Object hasStaffReview, Integer id, Image image, String name, List<Producer> producers, String rating, String releaseDate, String runtime, String siteDetailUrl, List<Studio> studios, String totalRevenue, List<Writer> writers) {
        super();
        this.apiDetailUrl = apiDetailUrl;
        this.boxOfficeRevenue = boxOfficeRevenue;
        this.budget = budget;
        this.dateAdded = dateAdded;
        this.dateLastUpdated = dateLastUpdated;
        this.deck = deck;
        this.description = description;
        this.id = id;
        this.image = image;
        this.name = name;
        this.producers = producers;
        this.rating = rating;
        this.releaseDate = releaseDate;
        this.runtime = runtime;
        this.siteDetailUrl = siteDetailUrl;
        this.studios = studios;
        this.totalRevenue = totalRevenue;
        this.writers = writers;
    }

    public String getApiDetailUrl() {
        return apiDetailUrl;
    }

    public void setApiDetailUrl(String apiDetailUrl) {
        this.apiDetailUrl = apiDetailUrl;
    }

    public String getBoxOfficeRevenue() {
        return boxOfficeRevenue;
    }

    public void setBoxOfficeRevenue(String boxOfficeRevenue) {
        this.boxOfficeRevenue = boxOfficeRevenue;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated(String dateLastUpdated) {
        this.dateLastUpdated = dateLastUpdated;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Producer> getProducers() {
        return producers;
    }

    public void setProducers(List<Producer> producers) {
        this.producers = producers;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getSiteDetailUrl() {
        return siteDetailUrl;
    }

    public void setSiteDetailUrl(String siteDetailUrl) {
        this.siteDetailUrl = siteDetailUrl;
    }

    public List<Studio> getStudios() {
        return studios;
    }

    public void setStudios(List<Studio> studios) {
        this.studios = studios;
    }

    public String getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(String totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public List<Writer> getWriters() {
        return writers;
    }

    public void setWriters(List<Writer> writers) {
        this.writers = writers;
    }
}

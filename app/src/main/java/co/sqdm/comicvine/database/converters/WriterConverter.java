package co.sqdm.comicvine.database.converters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import co.sqdm.comicvine.database.entities.Writer;

public class WriterConverter {
    @TypeConverter
    public List<Writer> stringToGson(String object){
        Gson gson = new Gson();
        Type writer = new TypeToken<List<Writer>>(){}.getType();
        return gson.fromJson(object, writer);
    }

    @TypeConverter
    public String gsonToString(List<Writer> object){
        Gson gson = new Gson();
        return gson.toJson(object);
    }
}
